#!/bin/sh

PATTERN=$1
if [ -z "$PATTERN" ]
then
	PATTERN="*_test"
fi

SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

python3 -m unittest discover -s test -p "$PATTERN.py"
