# Description #

A simple command line offline player for chat logs in the format you get from [rechat-dl](https://github.com/KunaiFire/rechat-dl) for TwitchTv.

# Dependencies #
* [Python3](https://www.python.org/)

# Usage #
## Direct ##
On a Unix-like system:

	chat_player.py chat_file start_time

On Windows:

	python chat_player.py chat_file start_time

## By creating a database file first (which requires much less memory during playback) ##
On a Unix-like system:

	to_db.py chat_file database_file.db
	chat_player.py database_file.db start_time

On Windows:

	python to_db.py chat_file database_file.db
	python chat_player.py database_file.db start_time
# Supported file types #
* .json - json files
* .json.gz - gzip compressed json files
* .json.zst - Zstandard compressed json files (if [zstd](https://facebook.github.io/zstd/) is installed)

# Examples #
Start replaying chat logs in a chat.json file from the beginning:

	chat_player.py chat.json

Start replaying chat logs in a chat.json file starting 2 minutes and 1 second in:

	chat_player.py chat.json 2:01

Start replaying chat logs in a chat.db file starting 4 hours 23 minutes and 8 seconds in:

	chat_player.py chat.db 4:23:08
