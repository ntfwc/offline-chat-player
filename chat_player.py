#!/usr/bin/python3

import os.path
import sys
import re
import sqlite3

from lib.ChatPlayer import ChatPlayer
from lib.printing import eprint
from lib.ChatDb import ChatDb,ReadOnlyChatDb
from to_db import to_db

TIME_REGEX=re.compile(r'(?:([0-9]{1,2}):)?([0-9]{1,2}):([0-9]{2})|([0-9]+(?:\.[0-9]+)?)')

def printUsage():
    eprint("Usage:")
    eprint("  chat_player.py chat_file [start_time]")
    eprint()
    eprint("chat_file: A file containing json formatted TwitchTv chat logs OR, if the extension is .db, a sqlite3 database of chat logs created with to_db.py")
    eprint("start_time: An offset time to start replaying logs at. This can be decimal seconds or a time in the format [HH:]MM:SS. If not specified, it starts replaying from the beginning.")

def parseTime(startTime):
    match = TIME_REGEX.fullmatch(startTime)
    if match == None:
        return None
    hours, minutes, seconds, floatSeconds = match.group(1,2,3,4)
    if (floatSeconds != None):
        return float(floatSeconds)
    startSeconds = int(seconds) + int(minutes) * 60
    if hours != None:
        startSeconds += int(hours) * 60 * 60
    return startSeconds

def play(chatDb, startSeconds):
    chatPlayer = ChatPlayer(chatDb)
    chatPlayer.run(startSeconds)

def main(argv):
    if len(argv) == 2:
        filePath = argv[1]
        startSeconds = 0
    elif len(argv) == 3:
        filePath = argv[1]
        startSeconds = parseTime(argv[2])
        if (startSeconds == None):
            eprint("Given time incorrectly formatted")
            printUsage()
            return 2
    else:
        printUsage()
        return 1

    if not os.path.exists(filePath):
        eprint("The given file '%s' does not exist" % filePath)
        return 3

    if filePath.endswith(".db"):
        with sqlite3.connect(filePath) as sqliteConn:
            play(ReadOnlyChatDb(sqliteConn), startSeconds)
    else:
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            to_db(filePath, chatDb)
            play(chatDb, startSeconds)

    return 0

if __name__ == "__main__":
    returnVal = main(sys.argv)
    exit(returnVal)
