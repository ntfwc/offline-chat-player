import unittest
import sqlite3

from lib.chat_reading import loadChatItems
from lib.ChatDb import ChatDb

NO_CHAT_JSON="test/resources/sample-no-chat.json"
class ChatReadingTest(unittest.TestCase):
    def testLoadChatItems_noChat(self):
        with open(NO_CHAT_JSON) as f, sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            loadChatItems(f, chatDb)
            self.assertEqual(0, chatDb.getItemCount())
