import unittest
import sqlite3
from lib.ChatDb import ChatDb,ReadOnlyChatDb,CHAT_LOG_TABLE,getTableNames
from lib.ChatItem import ChatItem

class ChatDbTest(unittest.TestCase):
    def testTableSetup(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            self.assertFalse(CHAT_LOG_TABLE in getTableNames(sqliteConn))
            chatDb = ChatDb(sqliteConn)
            self.assertTrue(CHAT_LOG_TABLE in getTableNames(sqliteConn))

    def testReadWithNothing(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            self.assertEqual(0, chatDb.getItemCount())
            reader = chatDb.getReader(0)
            self.assertIsNone(reader.fetchone())

    def testReadWithOneItem(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            chatItem = ChatItem(20.0, "user1", "Hi")
            chatDb.insertChatItem(chatItem)
            self.assertEqual(1, chatDb.getItemCount())
            reader = chatDb.getReader(0)
            self.assertEqual(chatItem, reader.fetchone())
            self.assertIsNone(reader.fetchone())

    def testReadWithOneItemUsingReadOnlyChatDb(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            chatItem = ChatItem(20.0, "user1", "Hi")
            chatDb.insertChatItem(chatItem)
            self.assertEqual(1, chatDb.getItemCount())
            reader = ReadOnlyChatDb(sqliteConn).getReader(0)
            self.assertEqual(chatItem, reader.fetchone())
            self.assertIsNone(reader.fetchone())

    def testReadWithTwoItems(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            chatItem1 = ChatItem(20.0, "user1", "Hi")
            chatDb.insertChatItem(chatItem1)
            chatItem2 = ChatItem(40.0, "user2", "Hi there")
            chatDb.insertChatItem(chatItem2)
            self.assertEqual(2, chatDb.getItemCount())
            reader = chatDb.getReader(0)
            self.assertEqual(chatItem1, reader.fetchone())
            self.assertEqual(chatItem2, reader.fetchone())
            self.assertIsNone(reader.fetchone())

    def testReadWithItemBeforeStartTime(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            chatItem = ChatItem(20.0, "user1", "Hi")
            chatDb.insertChatItem(chatItem)
            reader = chatDb.getReader(40.0)
            self.assertIsNone(reader.fetchone())

    def testReadWithStartTimeInMiddleOfItems(self):
        with sqlite3.connect(":memory:") as sqliteConn:
            chatDb = ChatDb(sqliteConn)
            chatItem1 = ChatItem(20.0, "user1", "Hi")
            chatDb.insertChatItem(chatItem1)
            chatItem2 = ChatItem(40.0, "user2", "Hi There")
            chatDb.insertChatItem(chatItem2)
            chatItem3 = ChatItem(60.0, "user2", "What's up?")
            chatDb.insertChatItem(chatItem3)
            self.assertEqual(3, chatDb.getItemCount())
            reader = chatDb.getReader(30.0)
            self.assertEqual(chatItem2, reader.fetchone())
            self.assertEqual(chatItem3, reader.fetchone())
            self.assertIsNone(reader.fetchone())
