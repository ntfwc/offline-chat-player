import unittest
import lib.time_formatting as time_formatting

class TimeFormattingTest(unittest.TestCase):
    def testFormatTimestamp(self):
        def assertResult(expected, inputVal):
            self.assertEqual(expected, time_formatting.formatTimestamp(inputVal))

        assertResult("0:00", 0)
        assertResult("0:01", 1)
        assertResult("0:10", 10)
        assertResult("0:59", 59)
        assertResult("1:00", 60)
        assertResult("1:01", 61)
        assertResult("1:59", 119)
        assertResult("2:00", 120)
        assertResult("20:00", 1200)
        assertResult("21:34", 1294)
        assertResult("1:00:00", 3600)
        assertResult("10:00:00", 36000)
        assertResult("10:02:59", 36179)
