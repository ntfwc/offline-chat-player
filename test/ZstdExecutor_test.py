import unittest
from lib.ZstdExecutor import getZstdExecutor
from subprocess import CalledProcessError

ZSTD_FILE="test/resources/example.zst"
NONEXISTENT_FILE="nonexistent12041.zst"
class ZstdExecutorTest(unittest.TestCase):
    def testDecompression(self):
        executor = self.__getExecutor()
        with executor.openDecompressionPipe(ZSTD_FILE) as p:
            self.assertEqual(b"some_data23912", p.read())

    def testDecompression_failsWithMissingFile(self):
        executor = self.__getExecutor()
        with self.assertRaises(CalledProcessError):
            with executor.openDecompressionPipe(NONEXISTENT_FILE) as p:
                p.read()

    def __getExecutor(self):
        executor = getZstdExecutor()
        self.assertIsNotNone(executor, "zstd not found, cannot run test :(")
        return executor
