import os
import subprocess
import platform
from contextlib import contextmanager

class ZstdExecutor:
    def __init__(self, exePath):
        self.exePath = exePath

    @contextmanager
    def openDecompressionPipe(self, fileName):
        process = subprocess.Popen([self.exePath, "-d", "-q", "-c", fileName], stdout=subprocess.PIPE)
        try:
            yield process.stdout
        except:
            raise
        else:
            process.stdout.close()
            try:
                process.wait(0.01)
            except:
                process.terminate()
                process.wait(1)
            if (process.poll()):
                process.terminate()
                process.wait(1)
            returncode = process.returncode
            if returncode != 0:
                raise subprocess.CalledProcessError(returncode, self.exePath)

def getZstdExecutor():
   """ Tries to find the zstd executable and create an executor
   @returns An executor, or null if the executable could not be found
   """
   exePath = __which("zstd")
   if exePath == None:
       return None
   return ZstdExecutor(exePath)

def __which(programName):
    if __isWindows():
        programName += ".exe"
    for path in os.environ["PATH"].split(os.pathsep):
        potentialFile = os.path.join(path, programName)
        if __is_exe(potentialFile):
            return potentialFile
    return None

def __is_exe(path):
    return os.path.isfile(path) and os.access(path, os.X_OK)

def __isWindows():
    return platform.system() == "Windows"

if __name__ == "__main__":
    executor = getZstdExecutor()
    for i in range(20):
        with executor.openDecompressionPipe("/tmp/blah.txt.zst") as p:
            print("bleh")
