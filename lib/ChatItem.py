class ChatItem:
    def __init__(self, offsetSeconds, username, text):
        self.offsetSeconds = offsetSeconds
        self.username = username
        self.text = text

    def __str__(self):
        return "ChatItem[offsetSeconds=%s, username=%s, text=%s]" % (self.offsetSeconds, self.username, self.text)

    def __eq__(self, other):
        if other is None:
            return self is None
        return self.__dict__ == other.__dict__
