import codecs
import json
from io import TextIOBase
from lib.ChatItem import ChatItem

def loadChatItems(fp, chatDb):
    if not isinstance(fp, TextIOBase):
        fp = codecs.getreader("utf-8")(fp)

    def object_hook(obj):
        return object_hook_with_chatdb(obj, chatDb)

    json.load(fp, object_hook=object_hook)

def object_hook_with_chatdb(obj, chatDb):
    if "content_offset_seconds" in obj:
        chatDb.insertChatItem(_toChatItem(obj))
        return None
    return obj

def _toChatItem(jsonItem):
    offsetSeconds = jsonItem["content_offset_seconds"]
    username = jsonItem["commenter"]["display_name"]
    text = jsonItem["message"]["body"]
    return ChatItem(offsetSeconds, username, text)
