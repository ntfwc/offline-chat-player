from lib.ChatItem import ChatItem

CHAT_LOG_TABLE="chat_log"

class ReadOnlyChatDb:
    def __init__(self, sqliteConn):
        self.sqliteConn = sqliteConn

    def getReader(self, startTime):
        cursor = self.sqliteConn.cursor()
        cursor.execute("SELECT * FROM " + CHAT_LOG_TABLE + " WHERE offsetSeconds >= ? ORDER BY offsetSeconds", (startTime * 1000,))
        return ChatItemReader(cursor)

    def getItemCount(self):
        cursor = self.sqliteConn.cursor()
        cursor.execute("SELECT COUNT(*) FROM " + CHAT_LOG_TABLE)
        return cursor.fetchone()[0]

class ChatDb(ReadOnlyChatDb):
    def __init__(self, sqliteConn):
        super().__init__(sqliteConn)
        self._initTables()

    def _initTables(self):
        tableNames = getTableNames(self.sqliteConn)
        if CHAT_LOG_TABLE not in tableNames:
            cursor = self.sqliteConn.cursor()
            cursor.execute("CREATE TABLE " + CHAT_LOG_TABLE + """(
            offsetSeconds INTEGER,
            username TEXT,
            text TEXT)""")

    def insertChatItem(self, chatItem):
        cursor = self.sqliteConn.cursor()
        cursor.execute("INSERT INTO " + CHAT_LOG_TABLE + " VALUES(?,?,?)", (int(chatItem.offsetSeconds * 1000), chatItem.username, chatItem.text))

class ChatItemReader:
    def __init__(self, cursor):
        self.cursor = cursor

    def fetchone(self):
        fetched = self.cursor.fetchone()
        if fetched == None:
            return None

        return ChatItem(float(fetched[0])/1000, fetched[1], fetched[2])

def getTableNames(sqliteConn):
    cursor = sqliteConn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'") 

    tableNames = []
    while True:
        fetched = cursor.fetchone()
        if fetched == None:
            break
        tableNames.append(fetched[0])
    return tableNames

