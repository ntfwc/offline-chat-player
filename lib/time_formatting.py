def formatTimestamp(seconds):
    seconds = int(seconds)
    if seconds < 60:
        return "0:%.2d" % seconds

    minutes = seconds // 60
    seconds = seconds % 60
    if minutes < 60:
        return "%d:%.2d" % (minutes, seconds)

    hours = minutes // 60
    minutes = minutes % 60
    return "%d:%.2d:%.2d" % (hours, minutes, seconds)
