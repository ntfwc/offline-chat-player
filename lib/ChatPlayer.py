import time
from lib.time_formatting import formatTimestamp

class ChatPlayer:
    def __init__(self, chatDb):
        self.chatDb = chatDb

    def run(self, seconds):
        chatReader = self.chatDb.getReader(seconds)
        self.startSeconds = seconds
        self.startTime = time.monotonic()

        while True:
            nextItem = chatReader.fetchone()
            if nextItem == None:
                return
            currentOffsetSeconds = self.__currentOffsetSeconds()
            if nextItem.offsetSeconds > currentOffsetSeconds:
                time.sleep(nextItem.offsetSeconds - currentOffsetSeconds)
            self.__printItem(nextItem)

    def __printItem(self, chatItem):
        print("[%s] %s: %s" % (formatTimestamp(chatItem.offsetSeconds), chatItem.username, chatItem.text))

    def __currentOffsetSeconds(self):
        currentTime = time.monotonic()
        return (currentTime - self.startTime) + self.startSeconds

    def __timeToNext(self, nextItemSeconds):
        currentTime = time.monotonic()
        currentOffsetSeconds = (currentTime - self.startTime)
