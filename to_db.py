#!/usr/bin/python3

import os.path
import sys
import gzip
import sqlite3
from lib.chat_reading import loadChatItems
from lib.ChatPlayer import ChatPlayer
from lib.printing import eprint
from lib.ZstdExecutor import getZstdExecutor
from lib.ChatDb import ChatDb

def printUsage():
    eprint("Usage:")
    eprint("  to_db.py chat_file sqlite_file.db")
    eprint()
    eprint("chat_file: A file containing json formatted TwitchTv chat logs")
    eprint("sqlite_file.db: A sqlite database file to create")

def to_db(filePath, chatDb):
    if filePath.endswith(".gz"):
        with gzip.open(filePath) as f:
            chatItems = loadChatItems(f, chatDb)
    elif filePath.endswith(".zst"):
        executor = getZstdExecutor()
        if executor == None:
            raise RuntimeError("Asked to open a .zst file, but zstd was not found")
        with executor.openDecompressionPipe(filePath) as p:
            chatItems = loadChatItems(p, chatDb)
    else:
        with open(filePath) as f:
            chatItems = loadChatItems(f, chatDb)

def main(argv):
    if len(argv) != 3:
        printUsage()
        return 1

    filePath = argv[1]
    sqlite_file = argv[2]

    if not sqlite_file.endswith(".db"):
        eprint("The sqlite file must end in .db")
        return 2

    if not os.path.exists(filePath):
        eprint("The given file '%s' does not exist" % filePath)
        return 3

    if os.path.exists(sqlite_file):
        eprint("The given sqlite file '%s' already exists" % sqlite_file)
        return 4

    with sqlite3.connect(sqlite_file) as sqliteConn:
        to_db(filePath, ChatDb(sqliteConn))

    return 0

if __name__ == "__main__":
    returnVal = main(sys.argv)
    exit(returnVal)
